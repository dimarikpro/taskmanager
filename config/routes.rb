Rails.application.routes.draw do
  devise_for :users
  root to: 'tasks#index'
  resources :tasks, shallow: true do
  	post 'start'
  	post 'finish'
    post 'cancel'
  end
end
