10.times do
  user = User.create(email: Faker::Internet.email, password: Faker::Internet.password(6))
  2.times do
    user.tasks.create(name: Faker::Name.name, description: Faker::Lorem.paragraph)
  end
end