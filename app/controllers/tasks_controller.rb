class TasksController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  before_action :load_task_id, only: [:start, :finish]
  before_action :load_task, only: [:show, :edit, :update, :destroy]

  respond_to :js, only: [:start, :finish, :cancel]
  respond_to :html

  def index
    respond_with(@tasks = Task.all.order('created_at'))
  end

  def show
    respond_with @task
  end

  def new
    respond_with(@task = Task.new)
  end

  def edit
  end

  def create
    respond_with(@task = Task.create(task_params))
  end

  def update
    @task.update(task_params)
    respond_with @task
  end

  def destroy
    @task.destroy if @task.user_id == current_user.id
    respond_with @task
  end

  def start
    @task.start_task
  end

  def finish
    @task.finish_task
  end

  private

  def load_task
    @task = Task.find(params[:id])
  end

  def load_task_id
    @task = Task.find(params[:task_id])
  end

  def task_params
    params.require(:task).permit(:name, :description, :state).merge(user_id: current_user.id)
  end
end
