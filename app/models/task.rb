class Task < ActiveRecord::Base
  #include ActiveModel::Validations
  belongs_to :user

  validates :user_id,     presence: true
  validates :name,        presence: true
  validates :description, presence: true, length: { minimum: 2, maximum: 1000 }
  validates :state,       presence: true, inclusion: { in: ['new', 'started', 'finished'] }

  def start_task
    update_attributes(state: 'started')
  end

  def finish_task
    update_attributes(state: 'finished')
  end
end
