FactoryGirl.define do
  factory :user do
    sequence(:email) { |i| "user#{i}@taskmanager.ru" }
    password '111111'
    password_confirmation '111111'
  end

end
