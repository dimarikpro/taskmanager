FactoryGirl.define do
  factory :task do
    name "MyString"
    description "MyText"
    state "new"
    user
  end

end
