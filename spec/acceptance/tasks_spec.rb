require_relative 'acceptance_helper'

feature 'Create task.' do
  given(:user)  { create(:user) }
  given(:user2) { create(:user) }
  given!(:task) { create(:task, user: user) }
  given(:task2) { create(:task, user: user2) }
  given(:tasks) { create_list(:task, 5, user: user) }

  scenario 'Authenticated user creates task' do
    sign_in(user)
    visit tasks_path
    click_on 'Новая задача'
    fill_in 'Наименование:', with: 'ййй'
    fill_in 'Описание:', with: 'й й й й й'
    click_on 'Добавить задачу'
    #save_and_open_page
    expect(page).to have_content 'Описание: й й й й й'
    expect(page).to have_content 'Состояние: new'
  end

  scenario 'Non-authenticated user try to create task' do
    visit new_task_path
    expect(page).to_not have_content 'Новая задача'
    expect(page).to have_content 'You need to sign in or sign up before continuing.'
  end

  scenario 'user can view task list' do
    tasks
    visit tasks_path
    tasks.each { |q| expect(page).to have_content q.name }
  end

  scenario 'Owner can delete task' do
    sign_in(user)
    visit task_path(task)
    click_on 'Удалить?'
    expect(page).to have_content 'Задач пока нет!'
  end

  scenario 'user can start someone task', js: true do
    sign_in(user2)
    within "#task_#{task.id}" do
      expect(page).to_not have_content 'Пуск задачи!'
      expect(page).to_not have_content 'Завершить задачу'
      expect(page).to_not have_content 'На доработку.'
    end
  end

  describe 'authorized user:', js: true do
    background { sign_in(user) }

    scenario 'user can start their task' do
      within "#task_#{task.id}" do
        click_on 'Пуск задачи!'
        expect(page).to have_content 'Завершить задачу'
      end
    end

    scenario 'user can start their task' do
      within "#task_#{task.id}" do
        click_on 'Пуск задачи!'
        click_on 'Завершить задачу'
        expect(page).to have_content 'На доработку.'
      end
    end
  end

end