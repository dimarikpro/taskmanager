require 'rails_helper'

RSpec.describe Task, type: :model do
  let(:user) { create(:user) }
  let!(:task) { create(:task, user: user) }

  it 'task.state' do
    expect(task.state).to eq 'new'
    task.start_task
    expect(task.state).to eq 'started'
    task.finish_task
    expect(task.state).to eq 'finished'
  end

end
